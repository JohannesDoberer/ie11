Luigi.setConfig({
    navigation: {
        nodes: [{
            pathSegment: 'home',
            label: 'h',
            children: [{
                pathSegment: 'overview',
                label: 'Home',
                icon: 'home',
                loadingIndicator: {
                    enabled: false
                },
                viewUrl: '/home.html'
            },
            {
                pathSegment: 'external',
                label: 'External',
                loadingIndicator: {
                    enabled: false
                },
                viewUrl: 'http://localhost:8081/index.html'
            }
            ]
        }]

    },
    communication: {
        customMessagesListeners: {
            'environment.created': function (data) {
                const settings = {
                    text: data.message,
                    type: 'info',
                    closeAfter: 3000
                }
                Luigi
                    .ux()
                    .showAlert(settings)
                    .then(function () {
                        // Logic to execute when the alert is dismissed
                    });
            }
        }
    }
})